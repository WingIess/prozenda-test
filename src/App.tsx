import { useEffect, useState, useMemo } from "react";
import { useInfiniteQuery, UseInfiniteQueryResult } from "@tanstack/react-query";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";

import { fetchStarWarsCharacters, IStarWarsCharacter, IStarWarsCharactersResponse, searchStarWarsCharacters } from "./api/people";
import CharacterCards from "./Components/CharacterCards/CharacterCards";
import SearchBox from "./Components/SearchBox/SearchBox";
import SortSelect, { A_Z, FEMALE, MALE, NONE, SortTypes, Z_A } from "./Components/SortSelect/SortSelect";

const extractNextPageParams = (prevResult: IStarWarsCharactersResponse) => {
    if (!prevResult.next) return null;
    const nextPage = prevResult.next.split("page=")[1];

    return parseInt(nextPage, 10);
};

const sortCharacterArrayByType = (characters: IStarWarsCharacter[], sortingType: SortTypes) => {
    const sortedCharacters = [...characters];
    switch (sortingType) {
        case A_Z:
            return sortedCharacters.sort((prevCharacter, nextCharacter) => {
                return prevCharacter.name > nextCharacter.name ? 1 : -1;
            });
        case Z_A:
            return sortedCharacters.sort((prevCharacter, nextCharacter) => {
                return prevCharacter.name < nextCharacter.name ? 1 : -1;
            });
        case MALE:
            return sortedCharacters.sort((prevCharacter, nextCharacter) => {
                return nextCharacter.gender === MALE && prevCharacter.gender !== MALE ? 1 : -1;
            });
        case FEMALE:
            return sortedCharacters.sort((prevCharacter, nextCharacter) => {
                return nextCharacter.gender === FEMALE && prevCharacter.gender !== FEMALE ? 1 : -1;
            });
        default:
            return characters;
    }
};

const App = () => {
    const [keyword, setKeyword] = useState("");
    const [sort, setSort] = useState<SortTypes>(NONE);
    const [currentQuery, setCurrentQuery] = useState<UseInfiniteQueryResult<IStarWarsCharactersResponse, unknown>>();

    const defaultCharacterQuery = useInfiniteQuery({
        enabled: !keyword,
        queryKey: ["character", "default"],
        getNextPageParam: extractNextPageParams,
        queryFn: ({ pageParam }) => fetchStarWarsCharacters(pageParam),
    });

    const searchCharacterQuery = useInfiniteQuery({
        enabled: !!keyword,
        queryKey: ["character", "search", keyword],
        getNextPageParam: extractNextPageParams,
        queryFn: ({ pageParam }) => searchStarWarsCharacters(keyword, pageParam),
    });

    useEffect(() => {
        if (!keyword) setCurrentQuery(defaultCharacterQuery);
    }, [defaultCharacterQuery.data, defaultCharacterQuery.isLoading, defaultCharacterQuery.isFetching, keyword]);

    useEffect(() => {
        if (!!keyword) setCurrentQuery(searchCharacterQuery);
    }, [searchCharacterQuery.data, searchCharacterQuery.isLoading, searchCharacterQuery.isFetching, keyword]);

    const characters = useMemo(() => currentQuery?.data?.pages.flatMap((character) => character.results) ?? [], [currentQuery?.data]);
    const nextPageAvailable = currentQuery?.data?.pages.at(-1)?.next;

    const sortedCharacters = useMemo(() => sortCharacterArrayByType(characters, sort), [characters, sort]);

    return (
        <div className="page main-frame">
            <SearchBox handleSubmit={setKeyword} />
            <div className="mb-20">
                Showing {characters.length} results of {currentQuery?.data?.pages.at(1)?.count}
            </div>
            <div className="mb-20">
                <SortSelect changeSorting={setSort} />
            </div>
            <div className="character-card-container">
                <CharacterCards characters={sortedCharacters} />
            </div>
            <div className="horizontal-center mt-20">
                {currentQuery?.isFetchingNextPage || currentQuery?.isLoading ? (
                    <CircularProgress />
                ) : (
                    <Button variant="contained" disabled={!nextPageAvailable} onClick={() => currentQuery?.fetchNextPage()}>
                        next page
                    </Button>
                )}
            </div>
        </div>
    );
};

export default App;
