import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";

import { IStarWarsCharacter } from "../../../api/people";

import image1 from "../../../assets/mock-image.png";
import image2 from "../../../assets/mock-image-1.png";

import styles from "./characterCard.module.scss";

interface CharacterCardsCustomProps {
    odd: boolean;
}

type HiddenStarWarsCharacterData =
    | "created"
    | "edited"
    | "url"
    | "height"
    | "mass"
    | "hair_color"
    | "skin_color"
    | "eye_color"
    | "birth_year"
    | "gender"
    | "homeworld"
    | "films"
    | "species"
    | "vehicles"
    | "starships";

type CharacterCardsProps = Omit<IStarWarsCharacter, HiddenStarWarsCharacterData> & CharacterCardsCustomProps;

const CharacterCard = ({ name, odd }: CharacterCardsProps) => (
    <Card>
        <CardMedia title="Jedi insignia">
            <img className={styles.cover_image} src={odd ? image1 : image2} />
        </CardMedia>
        <CardContent>
            <div className="horizontal-center">{name}</div>
        </CardContent>
    </Card>
);

export default CharacterCard;
