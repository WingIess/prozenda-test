import { IStarWarsCharacter } from "../../api/people";
import CharacterCard from "./CharacterCard/CharacterCard";

interface IProps {
    characters: IStarWarsCharacter[];
}

const CharacterCards = (props: IProps) => {
    return (
        <>
            {props.characters.map((character, index) => (
                <CharacterCard key={character.url} name={character.name} odd={index % 2 === 0} />
            ))}
        </>
    );
};

export default CharacterCards;
