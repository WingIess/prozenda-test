import * as React from "react";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";

export type SortTypes = "NONE" | "A-Z" | "Z-A" | "male" | "female";

export const A_Z = "A-Z";
export const Z_A = "Z-A";
export const MALE = "male";
export const FEMALE = "female";
export const NONE = "NONE";

interface PropTypes {
    changeSorting: (sortType: SortTypes) => void;
}

const SortSelect = (props: PropTypes) => {
    const [sort, setSort] = React.useState<SortTypes>(NONE);

    const handleChange = (event: SelectChangeEvent) => {
        setSort(event.target.value as SortTypes);
        props.changeSorting(event.target.value as SortTypes);
    };
    return (
        <Select value={sort} onChange={handleChange} displayEmpty inputProps={{ "aria-label": "Sort" }}>
            <MenuItem value={NONE}>sort by</MenuItem>
            <MenuItem value={A_Z}>A-Z</MenuItem>
            <MenuItem value={Z_A}>Z-A</MenuItem>
            <MenuItem value={MALE}>Male</MenuItem>
            <MenuItem value={FEMALE}>Female</MenuItem>
        </Select>
    );
};

export default SortSelect;
