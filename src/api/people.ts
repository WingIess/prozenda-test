import axios from "axios";

const baseUrl = "https://swapi.dev/api/people";

const starWarsCharacterClient = axios.create({ baseURL: baseUrl });

export interface IStarWarsCharacter {
    name: string;
    height: string;
    mass: 77;
    hair_color: string;
    skin_color: string;
    eye_color: string;
    birth_year: string;
    gender: string;
    homeworld: string;
    films: string[];
    species: string[];
    vehicles: string[];
    starships: string[];
    created: Date;
    edited: Date;
    url: string;
}

export interface IStarWarsCharactersResponse {
    count: number;
    next: string | null;
    previous: string | null;
    results: IStarWarsCharacter[];
}

export const fetchStarWarsCharacters = async (pageParam = 1) => {
    // await new Promise((res) => {
    //     setTimeout(() => res(0), 2000);
    // });
    const res = await starWarsCharacterClient.get<IStarWarsCharactersResponse>(`/?page=${pageParam}`);
    const data = res.data;
    return data;
};

export const searchStarWarsCharacters = async (keyword: string, pageParam = 1) => {
    const res = await starWarsCharacterClient.get<IStarWarsCharactersResponse>(`/?search=${keyword}&page=${pageParam}`);
    const data = res.data;
    return data;
};

export default starWarsCharacterClient;
