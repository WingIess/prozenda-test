import { defineConfig, HttpProxy, ProxyOptions } from "vite";
import react from "@vitejs/plugin-react-swc";

const logOutgoingRequests = (targetName: string) => {
    return (proxy: HttpProxy.Server, _options: ProxyOptions) => {
        proxy.on("error", (err, _req, _res) => {
            console.log(`${targetName} error`, err);
        });
        proxy.on("proxyReq", (proxyReq, req, _res) => {
            console.log(`Sending Request to ${targetName}:`, req.method, req.url);
        });
        proxy.on("proxyRes", (proxyRes, req, _res) => {
            console.log(`Received Response from ${targetName}:`, proxyRes.statusCode, req.url);
        });
    };
};

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    server: {
        port: 3000,
        proxy: {
            "https://swapi.dev/api/people/": {
                target: "https://swapi.dev/api/people/",
                changeOrigin: true,
                rewrite: (path) => path.replace("https://swapi.dev/api/people/", ""),
                secure: false,
                configure: logOutgoingRequests("deviant art"),
            },
        },
    },
});
